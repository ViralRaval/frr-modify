/* lib/version.h.  Generated from version.h.in by configure.
 *
 * Quagga version
 * Copyright (C) 1997, 1999 Kunihiro Ishiguro
 * 
 * This file is part of GNU Zebra.
 *
 * GNU Zebra is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * GNU Zebra is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Zebra; see the file COPYING.  If not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.  
 */

#ifndef _ZEBRA_VERSION_H
#define _ZEBRA_VERSION_H

#ifdef GIT_VERSION
#include "gitversion.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef GIT_SUFFIX
#define GIT_SUFFIX ""
#endif
#ifndef GIT_INFO
#define GIT_INFO ""
#endif

#define FRR_PAM_NAME    "frr"
#define FRR_SMUX_NAME   "frr"
#define FRR_PTM_NAME    "frr"

#define FRR_FULL_NAME   "FRRouting"
#define FRR_VERSION     "7.5" GIT_SUFFIX
#define FRR_VER_SHORT   "7.5"
#define FRR_BUG_ADDRESS "https://github.com/frrouting/frr/issues"
#define FRR_COPYRIGHT   "Copyright 1996-2005 Kunihiro Ishiguro, et al."
#define FRR_CONFIG_ARGS "'--target=arm-openwrt-linux' '--host=arm-openwrt-linux' '--build=x86_64-linux-gnu' '--program-prefix=' '--program-suffix=' '--exec-prefix=/usr' '--bindir=/usr/bin' '--sbindir=/usr/sbin' '--libexecdir=/usr/lib' '--sysconfdir=/etc' '--datadir=/usr/share' '--localstatedir=/var' '--mandir=/usr/man' '--infodir=/usr/info' '--disable-nls' '--with-clippy=/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/target-arm_cortex-a7_musl-1.1.16_eabi/host/bin/clippy' '--prefix=/usr' '--enable-shared' '--disable-static' '--enable-user=network' '--enable-group=network' '--disable-ospfclient' '--disable-doc' '--disable-backtrace' '--localstatedir=/var/run/frr' '--sysconfdir=/etc/frr/' '--disable-babeld' '--disable-bfdd' '--enable-bgpd' '--disable-eigrpd' '--disable-fabricd' '--disable-isisd' '--disable-ldpd' '--disable-nhrpd' '--enable-ospfd' '--enable-ospf6d' '--disable-pbrd' '--disable-pimd' '--enable-ripd' '--disable-ripngd' '--enable-staticd' '--disable-vrrpd' '--enable-vtysh' '--enable-zebra' 'build_alias=x86_64-linux-gnu' 'host_alias=arm-openwrt-linux' 'target_alias=arm-openwrt-linux' 'PKG_CONFIG=/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/host/bin/pkg-config' 'PKG_CONFIG_PATH=/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/target-arm_cortex-a7_musl-1.1.16_eabi/usr/lib/pkgconfig:/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/target-arm_cortex-a7_musl-1.1.16_eabi/usr/share/pkgconfig' 'PKG_CONFIG_LIBDIR=/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/target-arm_cortex-a7_musl-1.1.16_eabi/usr/lib/pkgconfig:/home/viral/bob-the-builder/SDWAN_QNTM/qsdk-11.4/qsdk/staging_dir/target-arm_cortex-a7_musl-1.1.16_eabi/usr/share/pkgconfig' 'CC=arm-openwrt-linux-muslgnueabi-gcc' 'CXX=arm-openwrt-linux-muslgnueabi-g++'"

#define FRR_DEFAULT_MOTD \
	"\n" \
	"Hello, this is " FRR_FULL_NAME " (version " FRR_VERSION ").\n" \
	FRR_COPYRIGHT "\n" \
	GIT_INFO "\n"

pid_t pid_output (const char *);

#ifdef __cplusplus
}
#endif

#endif /* _ZEBRA_VERSION_H */
